# nfs4_server Ansible role

Install and configure an NFS v4 server on Centos 7+. NFS versions older than v4 are NOT supported.

Assumes the use of firewalld.

Note; work has been started for Debian based hosts, but this has not been completed.

Requirements
------------
Ansible 2.5 or later

Role Variables
--------------

**nfs_allow_cidr** - Source IP CIDR for firewalld. Defaults to 127.0.0.0/8. You **will** want to change this.

**exports** - List of export path, client and option pairs. See /etc/exports for more information.

Dependencies
------------

None


Example Playbook
----------------
```
- hosts: nfs
  become: yes
  vars:
    exports:
    - path: "/export/home"
      options: "*(rw,sync,no_root_squash,no_all_squash)"
  roles:
  - nfs4_server

- hosts: nfs
  tasks:
  - name: Install NFSv4 server
    include_role:
      name: nfs4_server
    vars:
      nfs_allow_cidr: "{{ allow_cidr }}"
      exports:
        - path: "{{ nfs_path }}"
          clients:
          - client: "{{ client_cidr }}"
            options: "{{ nfs_opts }}"

```

License
-------

MIT

Author Information
------------------
Paul T Sparks
